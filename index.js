var daySo = [];


// funtion 1 
function laySo() {
    var soNhap = Number(dom('soNhap').value);
    daySo.push(soNhap);
    dom('daySo').innerHTML = daySo;
}

// Tổng các số dương trong mảng
function Tinh1() {
    var ketQua1 = 0;
    for (i = 0; i < daySo.length; i++) {
        if (daySo[i] > 0) {
            ketQua1 += daySo[i]
        };
    };
    dom('ketQua1').innerHTML = ketQua1;
}

// Đếm có bao nhiêu số dương trong mảng
function Tinh2() {
    var ketQua2 = 0;
    for (i = 0; i < daySo.length; i++) {
        if (daySo[i] > 0) {
            ketQua2++;
        };
    };
    dom('ketQua2').innerHTML = ketQua2;
}

// Tìm số nhỏ nhất trong mảng
function Tinh3() {
    var ketQua3 = 0;
    for (i = 0; i < daySo.length; i++) {
        if (daySo[i] >= ketQua3) {
            ketQua3 = daySo[i];
        };
    };
    dom('ketQua3').innerHTML = ketQua3;
}

// Tìm số dương nhỏ nhất trong mảng
function Tinh4() {
    var ketQua4 = daySo[0];
    for (i = 0; i < daySo.length; i++) {
        if (daySo[0] > 0 && daySo[i] <= ketQua4) {
            ketQua4 = daySo[i];
        };
    };
    if (ketQua4 > 0) {
        dom('ketQua4').innerHTML = ketQua4;
    } else {
        dom('ketQua4').innerHTML = 'không có số nguyên dương';
    }
}

// 5. Tìm số chẵn cuối cùng trong mảng. Nếu mảng không có giá trị chẵn thì trả về -1.
function Tinh5() {
    for (i = 0; i < daySo.length; i++) {
        if (daySo[i] % 2 == 0) {
            ketQua5 = daySo[i];
        };
    }

    if (ketQua5 = '[object HTMLSpanElement]') {
        dom('ketQua5').innerHTML = -1;
    } else {
        dom('ketQua5').innerHTML = ketQua5;
    }
}

// Đổi chỗ 2 giá trị trong mảng theo vị trí (Cho nhập vào 2 vị trí muốn đổi chỗ giá trị).
function Tinh6() {
    viTri1 = Number(dom('viTri1').value);
    viTri2 = Number(dom('viTri2').value);

    if (viTri1 < 0 || viTri1 > daySo.length) {
        alert('Vị trí 1 nhập sai')
    }
    if (viTri2 < 0 || viTri2 > daySo.length) {
        alert('Vị trí 2 nhập sai')
    }
    a = daySo[viTri1];
    daySo[viTri1] = daySo[viTri2];
    daySo[viTri2] = a;

    dom('ketQua6').innerHTML = daySo;

}

// Sắp xếp mảng theo thứ tự tăng dần
function Tinh7() {
    daySo.sort();
    dom('ketQua7').innerHTML = daySo;


}

// Tìm số nguyên tố đầu tiên trong mảng. Nếu mảng không có số nguyên tố thì trả về – 1.
function Tinh8() {
    for (i = 0; i < daySo.length; i++) {
        if (daySo[i] == 2){
            ketQua8 = 2;
            break
        } else if (check(daySo[i])){
            ketQua8 = daySo[i];
            break;
        } else{
            ketQua8 = -1;
        }
    }
    dom('ketQua8').innerHTML = ketQua8;
}

// Nhập thêm 1 mảng số thực, tìm xem trong mảng có bao nhiêu số nguyên?
function Tinh9() {
    ketQua9 = 0;
    for (i = 0; i < daySo.length; i++) {
        if (daySo[i] % 1 == 0){
            ketQua9++ ;
    }
    dom('ketQua9').innerHTML = ketQua9;}
}

// So sánh số lượng số dương và số lượng số âm
function Tinh10() {
    soDuong = 0;
    soAm = 0;
    for (i = 0; i < daySo.length; i++) {
        if (daySo[i] > 0) {
            soDuong++;
        }
        if (daySo[i] < 0) {
            soAm++;
        }
    }

    console.log(soAm);
    console.log(soDuong);

    if (soAm == soDuong) {
        dom('ketQua10').innerHTML = 'Số âm bằng số Dương';
    } else if (soAm > soDuong) {
        dom('ketQua10').innerHTML = 'Số âm lớn hơn số Dương';
    } else {
        dom('ketQua10').innerHTML = 'Số âm nhỏ hơn số Dương';
    }
}



// method 
function dom(id) {
    return document.getElementById(id);
}

// check số nguyên tố
function check(n) {
    a = true;
    for (x = 2; x <= Math.sqrt(n); x++) {
        if (n % x == 0) {
            a = false;
            break;
        } else {
            a = true;
        }
    }

    return a;
}
